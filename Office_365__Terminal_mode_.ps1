﻿$Host.UI.RawUI.WindowTitle = "Instalador do Office365"
$ErrorActionPreference = "SilentlyContinue"

Write-Host "`nLEIA COM ATENÇÃO AS INSTRUÇÕES ABAIXO:

> O perfil do usuário precisa ter privilégios de administrador para que o instalador funcione adequadamente;
> É fundamental que todos os resquícios de Office sejam removidos antes da instalação do Office365;
> A inclusão do Access resultará numa reinstalação do Office365;
> Caso ocorra algum erro na instalação, execute o removedor através do menu de instalação;
> Este instalador cria uma pasta com os arquivos de instalação na pasta C:\Temp;
> Este instalador cria um log em C:\Windows\Logs" -ForegroundColor Red -BackgroundColor Black

Write-Host -NoNewLine "`nPressione qualquer tecla para continuar...`n";
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

Write-Host "`nFinalizando processos do Office..." -ForegroundColor Red -BackgroundColor Black; Start-Sleep -s 1 
Stop-Process -force -ProcessName WINWORD
Stop-Process -force -ProcessName MSACCESS
Stop-Process -force -ProcessName ONENOTEM
Stop-Process -force -ProcessName ONENOTE
Stop-Process -force -ProcessName OUTLOOK
Stop-Process -force -ProcessName EXCEL
Stop-Process -force -ProcessName POWERPNT

cmd /c reg add hkcu\software\microsoft\office\16.0\common\logging /v enablelogging /t reg_dword /d 1 /f
cmd /c reg add HKLM\SOFTWARE\Microsoft\ClickToRun\OverRide /v LogLevel /t REG_DWORD /d 3 /f
cmd /c reg add HKLM\SOFTWARE\Microsoft\ClickToRun\OverRide /v PipelineLogging /t REG_DWORD /d 1 /f

function check_dir {

     $dir = "C:\temp\OFFICE365"
     $infra = Test-Path -Path "\\infra\INFRA-TIC"

     if (Test-Path -Path $dir) {
          Remove-Item -Recurse -Force $dir
          New-Item -Path c:\temp\Office365 -ItemType Directory -ErrorVariable permissao
          
          if ($permissao){
               Write-Warning -Message "Verifique se o usuário possui privilégios de administrador e tente novamente."
               quit
          }

          else{
               Write-Output "`nCopiando os arquivos de instalação..."
               Copy-Item "\\infra\infra-tic\ATENDIMENTO\PROGRAMAS\OFFICE365\setup.exe" -Destination C:\temp\OFFICE365

               if ($infra -eq $false){
                    Write-Warning -Message "Verifique a conexão com a rede e tente novamente"
                    quit
               }

               else{
                    Write-Output "`nArquivos de instalação foram copiados para a pasta C:\Temp\OFFICE365`n"
                    Start-Sleep -s 1
                    Set-Location C:\Temp\OFFICE365
               }
          }

          
     }

     else {
          New-Item -Path $dir -ItemType Directory -ErrorVariable permissao

          if ($permissao) {
               Write-Warning -Message "Verifique se o usuário possui privilégios de administrador e tente novamente."
               quit
          }

          else {
               Write-Output "`nCopiando os arquivos de instalação..."
               Copy-Item "\\infra\infra-tic\ATENDIMENTO\PROGRAMAS\OFFICE365\setup.exe" -Destination C:\temp\OFFICE365

               if ($infra -eq $false) {
                    Write-Warning -Message "Verifique a conexão com a rede e tente novamente."
                    quit    
               }

               else {
                    Write-Output "`nArquivos de instalação foram copiados para a pasta C:\Temp\OFFICE365`n"
                    Start-Sleep -s 1
                    Set-Location C:\Temp\OFFICE365
               }
          }
   
     }
}

function uninstaller {
     
     $rem = "C:\Temp\Removedor_Office"
     $infra = Test-Path -Path "\\infra\INFRA-TIC"

     if (Test-Path -Path $rem) {
          Remove-Item -Recurse -Force $rem -ErrorVariable permissao
          New-Item -Path C:\temp\OFFICE365 -ItemType Directory -ErrorVariable permissao

          if ($permissao) {
               Write-Warning -Message "Verifique se o usuário possui privilégios de administrador e tente novamente."
               quit
          }

          else {
               Write-Output "`nCopiando o removedor do Office..."
               Copy-Item "\\Infra\infra-tic\ATENDIMENTO\PROGRAMAS\Microsoft Fix\Remoção\O15CTRRemove.diagcab" -Destination C:\Temp\Removedor_Office

               if ($infra -eq $false) {
                    Write-Warning -Message "Verifique a conexão com a rede e tente novamente"
                    quit
               }

               else {
                    Write-Output "`nO removedor foi copiado para a pasta C:\Temp\Removedor_Office"
                    Start-Sleep -s 1
               }

                    
          }
          
     }

     else{
          New-Item -Path $rem -ItemType Directory -ErrorVariable permissao

          if ($permissao) {
               Write-Warning -Message "Verifique se o usuário possui privilégios de administrador e tente novamente."
               quit
          }

          else {
               Write-Output "`nCopiando o removedor do Office..."
               Copy-Item "\\Infra\infra-tic\ATENDIMENTO\PROGRAMAS\Microsoft Fix\Remoção\O15CTRRemove.diagcab" -Destination C:\Temp\Removedor_Office -ErrorVariable rede

               if ($infra -eq $false) {
                    Write-Warning -Message "Verifique a conexão com a rede e tente novamente"
                    quit
               }

               else {
                    Write-Output "`nO removedor foi copiado para a pasta C:\Temp\Removedor_Office" ; Start-Sleep -s 1
                    Write-Host "`nAguarde o removedor ser executado. Não feche este janela enquanto a remoção estiver sendo feita."
                    Set-Location C:\temp\Removedor_Office
                    .\O15CTRRemove.diagcab ; Start-Sleep -s 3
                    
                    $text = "O instalador executou o arquivo O15CTRRemove.diagcab."
                    log
               }

          }
     }
}

function ox64 {

[xml]$confx64 = @"
     <Configuration>
         <Add OfficeClientEdition="64" Channel="Monthly" OfficeMgmtCOM="TRUE" ForceUpgrade="TRUE" MigrateArch="TRUE">
          <Product ID="O365ProPlusRetail">
                 <Language ID="pt-br" />
                 <Language ID="en-us" />
                 <Language ID="es-es" />
                    <ExcludeApp ID="Groove" />
                 <ExcludeApp ID="Publisher" />
              </Product>
         </Add>
         <Property Name="AUTOACTIVATE" Value="0" />
         <AppSettings>
             <Setup Name="Company" Value="PETROBRAS" />
         </AppSettings>
         <RemoveMSI />
         <Display Level="Full" AcceptEULA="TRUE" />
         <Logging Level="Standard" Path="C:\windows\logs\Office365_petro" /> 	
     </Configuration>
"@
$confx64.Save('C:\Temp\OFFICE365\configuration_64.xml')

     $access = Read-Host "Deseja incluir o Access na instalação? [s/n]"

          if ($access -eq "s") {
               Write-Host "Iniciando a instalação..." -ForegroundColor Yellow ; Start-Sleep -s 1
               .\setup.exe /configure configuration_64.xml

               $text = "O instalador executou o comando .\setup.exe /configure configuration_64.xml. O Access foi adicinado ao Pacote Office365."
               log

               Set-Location $Env:USERPROFILE\desktop\
          }

          elseif ($access -eq "n") {

               $semaccess = [xml] (Get-Content C:\Temp\OFFICE365\configuration_64.xml)
               $settings = New-Object System.Xml.XmlWriterSettings
               $settings.Indent = $true
               $settings.IndentChars = "t"
               $settings.NewLineChars = "r`n"

               $product = $semaccess.SelectSingleNode('//Product[@ID="O365ProPlusRetail"]')

               'Access' | ForEach-Object {
                    $node = $semaccess.CreateElement('ExcludeApp')
                    $node.SetAttribute('ID', $_)
                    [void]$product.AppendChild($node)
                }

                $semaccess.Save("C:\Temp\OFFICE365\configuration_64_sem_Access.xml")

               Write-Host "Iniciando a instalação..." -ForegroundColor Yellow ; Start-Sleep -s 1
               .\setup.exe /configure configuration_64_sem_Access.xml

               $text = "O instalador executou o comando .\setup.exe /configure configuration_64_sem_Access. O Access não foi adicinado ao Pacote Office365."
               log

               Set-Location $Env:USERPROFILE\desktop\
          }

          else {
               Write-Host "Selecione uma opção válida!" -ForegroundColor Red ; Start-Sleep -s 1
               return ox64
          }
     
}

function ox86 {

[xml]$confx86 = @"
     <Configuration>
         <Add OfficeClientEdition="32" Channel="Monthly" OfficeMgmtCOM="TRUE" ForceUpgrade="TRUE" MigrateArch="TRUE">
          <Product ID="O365ProPlusRetail">
                 <Language ID="pt-br" />
                    <ExcludeApp ID="Access" />
                 <ExcludeApp ID="Groove" />
                 <ExcludeApp ID="Lync" />
                 <ExcludeApp ID="Publisher" />
                 <ExcludeApp ID="Teams" />
             </Product>
         </Add>
         <Updates Enabled="TRUE" />
         <RemoveMSI />
         <AppSettings>
             <Setup Name="Company" Value="PETROBRAS" />
         </AppSettings>
         <Display Level="full" AcceptEULA="TRUE" />
         <Logging Level="Standard" Path="C:\windows\logs\Office365_petro" /> 	
     </Configuration>
"@
$confx86.Save('C:\Temp\OFFICE365\Configuration_32_sem_Access.xml')

     $access = Read-Host "Deseja incluir o Access na instalação? [s/n]"

          if ($access -eq "s") {

               (Get-Content -path c:\temp\OFFICE365\Configuration_32_sem_Access.xml -Raw) -replace '<ExcludeApp ID="Access" />', '' | Set-Content c:\temp\OFFICE365\configuration_32.xml
               Write-Host "Iniciando a instalação..." -ForegroundColor Yellow ; Start-Sleep -s 1
               .\setup.exe /configure configuration_32.xml

               $text = "O instalador executou o comando .\setup.exe /configure configuration_32.xml. O Access foi adicinado ao Pacote Office365."
               log

               Set-Location $Env:USERPROFILE\desktop\
          }

          elseif ($access -eq "n") {

               Write-Host "Iniciando a instalação..." -ForegroundColor Yellow ; Start-Sleep -s 1
               .\setup.exe /configure configuration_32_sem_Access.xml
               
               $text = "O instalador executou o comando .\setup.exe /configure configuration_32_sem_Access.xml. O Access não foi adicinado ao Pacote Office365."
               log

               Set-Location $Env:USERPROFILE\desktop\
          }

          else {
               Write-Host "Selecione uma opção válida!" -ForegroundColor Red ; Start-Sleep -s 1
               return ox86
          }
     
     
}

function license {

     Import-Module ActiveDirectory -ErrorVariable noad

     if ($noad) {
          Write-Warning -Message "Não foi possível carregar o Active Directory. Ele pode não estar instalado ou defeituoso." ; Start-Sleep -s 1
          quit
     }

     else {
          $user = Read-Host "`nDigite a chave do usuário"
          $username = Get-ADUser $user -Properties DIsplayName -ErrorVariable notfind | Select-Object DIsplayName

          if ($notfind) {
               Write-Warning -Message "`nChave não localizada no Active Directory. Tente novamente.`n" ; Start-Sleep -s 2
               return license
          }

          elseif ($username -like "* - PrestServ*") {
               Write-Host "`nUsuário"$user.ToUpper()"não possui licença do Office365." -ForegroundColor Red
               Write-Host "`nLicença disponível somente para funcionários Petrobras." -ForegroundColor Red
               Start-sleep -s 2
               return selec
          }

          elseif ($user -notlike "* - PrestServ*") {
               Write-Host "`nUsuário"$user.ToUpper()"possui licença do Office365." -ForegroundColor Green ; Start-sleep -s 2
               return selec 
          }

          else {
               Write-Host "`nAlgo deu errado. Tente novamente."
               return license
          }
     }
     
}

function quit {

     Write-Output "`nEncerrando o instalador..."
     Set-Location $Env:USERPROFILE\desktop\
     Remove-Item -Recurse -Force C:\temp\OFFICE365
     Remove-Item -Recurse -Force C:\temp\Removedor_Office
     Set-Location HKCU:\SOFTWARE\Microsoft\office\16.0\common\logging 
     Remove-ItemProperty -Force -Path "HKCU:\SOFTWARE\Microsoft\office\16.0\common\logging" -Name "enablelogging" 
     Set-Location HKLM:\SOFTWARE\Microsoft\ClickToRun\OverRide
     Remove-ItemProperty -Force -Path "HKLM:\SOFTWARE\Microsoft\ClickToRun\OverRide" -Name "LogLevel"
     Remove-ItemProperty -Force -Path "HKLM:\SOFTWARE\Microsoft\ClickToRun\OverRide" -Name "PipelineLogging"
     Set-Location $Env:USERPROFILE\desktop\
     Start-Sleep -s 1
     return
}

function repeat_if_u_want {

     $option = Read-Host "`nDigite 'p' caso queira voltar para o menu ou qualquer outra tecla para fechar o instalador"

     if ($option -eq 'p') { selec }

     else { quit }
}

function log {
     $directory = "C:\Windows\Logs\Office365_instalador.txt" 
     $date = Get-Date -format "dd/MM/yyyy, HH:mm:ss:"
     $msg = "$date $text"

     if ($directory) { $msg | Add-Content $directory }

     else { $msg | Out-File $directory }
}

function selec {
     param (
          [string]$Titulo = 'Menu'
     )

     Write-Host "`n============================ Menu de Instalação ============================`n"
     Write-Host "	[1] para instalar o Office365 64bits"
     Write-Host "	[2] para instalar o Office365 32bits"
     Write-Host "	[r] para remover todos os resquícios do Office"
     Write-Host "	[l] para verificar se o usuário possui licença (executar na chave do técnico)" 
     Write-Host "	[q] para fechar o instalador"
     Write-Host "`n============================================================================"

     $selection = Read-Host "`nSelecione uma das opções acima"
     switch ($selection) {

          '1' { 

               Write-Host "-> Instalação da versão 64bits selecionada" -ForegroundColor Green ; Start-Sleep -s 1
               check_dir
               ox64

               repeat_if_u_want
           }


          '2' {

               Write-Host "-> Instalação da versão 32bits selecionada" -ForegroundColor Yellow ; Start-Sleep -s 1
               check_dir
               ox86

               repeat_if_u_want
          } 

          'r' {

               Write-Host "`nVocê selecionou remover os resquícios do Office." -ForegroundColor Red; Start-Sleep -s 1
               Write-Host "`n>>>>Atenção!!" -ForegroundColor Red;
               Write-Host "`n>> Caso o sistema possua o Project ou o Visio instalados, será necessário reinstalá-los após este procedimento." -ForegroundColor Red
               Write-Host ">> O desinstalador remove todos os resquícios do Office." -ForegroundColor Red; Start-Sleep -s 3
               Write-Host -NoNewLine "`nPressione qualquer tecla para continuar...`n";
               $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

               uninstaller

               repeat_if_u_want
          }

          'l' { license }

          'q' { quit }

          Default {

               if ($selection -ige 3 -or $selection -ne 'q', 'r', 'l') {
               Write-Host "`n>>> Selecione apenas opções que estejam no menu!`n" -ForegroundColor Red
               Start-Sleep -s 2
               return selec
          }

     }
}}

$path = New-Item -Path "c:\temp\OFFICE365" -ErrorVariable perm ; if ($path) {Remove-Item -Recursive -Force $path}
$rede = Test-Path "\\infra\infra-tic\"

if ($perm){Write-Warning -Message "`nVerifique se o usuário possui privilégios de administrador e tente novamente." ; quit ; Start-Sleep -s 4}

elseif ($rede -eq $false){Write-Warning -Message "`nVerifique a conexão com a rede e tente novamente" ; quit ; Start-Sleep -s 4}

else {selec}

<#.NOTES
Versão:            3.0
Autor:             BJBD
Data da atualização:   18/05/2020
Propósito:         Facilitar e agilizar a instalação do Pacote Office365.
Agradecimentos para:
- Meu irmão, Igor, por me deixar usar o computador dele para criar as primeiras versões do instalador;
- A Caio (AAY2), por ter atendido tanto Office 365 ao ponto de me fazer ficar cansado de resolver os atendimentos da mesma forma;
- Aos colegas de trabalho que usam o instalador sempre que possível, e sempre dão um feedback sobre o funcionamento (felizmente, maioria positivo);
- Aos meus 'polêmicos' companheiros de equipe da Infiniti: Estão gostando!? #pld10.#>